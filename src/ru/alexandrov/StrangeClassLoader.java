package ru.alexandrov;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StrangeClassLoader extends ClassLoader {

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bt = loadClassData(name);
        return defineClass(name, bt, 0, bt.length);
    }

    private byte[] loadClassData(String className) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(className.replace(".", "/")+".class");
        if (is == null) {
            throw new RuntimeException("Class not found");
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = 0;
        try {
            while((len=is.read())!=-1){
                baos.write(len);
            }
        } catch (IOException e){
            System.out.println("An error with loading class data");
        }
        return baos.toByteArray();
    }
}
