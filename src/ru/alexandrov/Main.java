package ru.alexandrov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
	    StrangeClassLoader loader = new StrangeClassLoader();
	    Class<?> clazz = loader.findClass("ru.alexandrov.Test");
	    Object o = clazz.getConstructor().newInstance();
	    Method method = clazz.getMethod("show");
	    method.invoke(o);
    }
}
